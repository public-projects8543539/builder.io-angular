import { Component } from '@angular/core';
import { LocalStorageService } from '../local-storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent {
  constructor(
    private localStorageService: LocalStorageService,
    private router: Router) { }

  ngOnInit(): void {
    // Validar el login al inicializar el componente
    this.localStorageService.validarLogin().subscribe(isLoggedIn => {
      if (!isLoggedIn) {
        // Redirigir al usuario a la ruta '/'
        this.router.navigate(['']);
      }
    });
  }
}
