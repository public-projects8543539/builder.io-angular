import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private router: Router) { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    console.log('en interceptor');
    // Verificar si existe el key 'login' en localStorage con el valor '1'
    if (localStorage.getItem('login') !== '1') {
      // Redirigir al usuario a la ruta '/'
      this.router.navigate(['/']);
    }
    // Si el usuario está autenticado, continuar con la solicitud
    return next.handle(request);
  }
}
