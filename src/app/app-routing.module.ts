import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetailsComponent } from './details/details.component';
import { LoginComponent } from './login/login.component';
import { authGuard } from './guard/auth.guard';
import { TableroComponent } from './tablero/tablero.component';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent,
    title: 'Home page'
  },
  {
    path: 'details',
    component: DetailsComponent,
    title: 'Home details',
    canActivate: [authGuard]
  },
  {
    path: 'tablero',
    component: TableroComponent,
    title: 'Tablero',
    canActivate: [authGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
