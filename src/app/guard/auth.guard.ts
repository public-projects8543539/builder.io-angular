import { inject } from '@angular/core';
import { CanMatchFn, Router } from '@angular/router';
import { LocalStorageService } from '../local-storage.service';

export const authGuard: CanMatchFn = (route, state) => {
  const localStorageService = inject(LocalStorageService);
  return localStorageService.validarLogin()
};
