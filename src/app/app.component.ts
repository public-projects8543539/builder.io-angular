import { Component } from '@angular/core';
import { LocalStorageService } from './local-storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'tableroProsa';

  constructor(private localStorageService: LocalStorageService,
    private router: Router
  ) { }

  eliminarValorLocalStorage(): void {
    this.localStorageService.eliminarValor('login');
    this.router.navigate(['/']);
  }

}
