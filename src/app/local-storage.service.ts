import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor() { }

  guardarValor(key: string, valor: any): void {
    localStorage.setItem(key, JSON.stringify(valor));
  }

  eliminarValor(key: string): void {
    localStorage.removeItem(key);
  }

  validarLogin(): Observable<boolean> {
    const loginValue = localStorage.getItem('login');
    // Verificar si el key 'login' existe y su valor es '1'
    if (loginValue && loginValue === '1') {
      return of(true); // Retornar un Observable con el valor true
    } else {
      return of(false); // Retornar un Observable con el valor false si no se cumple la condición
    }
  }

}
