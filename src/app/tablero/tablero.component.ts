import { Component } from '@angular/core';

@Component({
  selector: 'app-tablero',
  template: `
  <builder-component
  *ngIf="!noBuilderPageForUrl"
  model="tablero"
  (load)="noBuilderPageForUrl = $event ? false : true"
  (error)="noBuilderPageForUrl = true">
  <!-- Default content inside the tag shows while the builder content is fetching -->
  <div class="spinner"></div>
  </builder-component>
  `,
  styleUrls: ['./tablero.component.scss']
})
export class TableroComponent {
  noBuilderPageForUrl: boolean = false;
}
