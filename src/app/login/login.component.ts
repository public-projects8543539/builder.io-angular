import { Component, inject } from '@angular/core';
import { FormControl, FormGroup, FormsModule } from '@angular/forms';
import { LocalStorageService } from '../local-storage.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  formulario: FormGroup
  snackBar = inject(MatSnackBar)

  constructor(
    private localStorageService: LocalStorageService,
    private router: Router
  ) {
    this.formulario = new FormGroup({
      username: new FormControl(),
      password: new FormControl()
    })
  }

  onSubmit() {
    const user = this.formulario.value;
    if (user.username == "admin" && user.password == "prosa.admin05") {
      this.localStorageService.guardarValor('login', 1);
      this.router.navigate(['/details']);
    } else {
      this.snackBar.open('Incorrect username or password', 'Close');
    }
  }

}
